package com.task.financer.domain;

import javax.persistence.*;

@Entity
public class Notes {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nameNotes;
    private String textNotes;


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User author;

    public Notes() {
    }

    public Notes(String nameNotes, String textNotes, User user) {
        this.author = user;
        this.nameNotes = nameNotes;
        this.textNotes = textNotes;
    }


    public String getAuthorName() {
        return author != null ? author.getUsername() : "<none>";
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setNameNotes(String nameNotes) {
        this.nameNotes = nameNotes;
    }

    public void setTextNotes(String textNotes) {
        this.textNotes = textNotes;
    }

    public Long getId() {
        return id;
    }

    public String getNameNotes() {
        return nameNotes;
    }

    public String getTextNotes() {
        return textNotes;
    }
}
