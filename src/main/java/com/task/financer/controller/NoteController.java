package com.task.financer.controller;

import com.task.financer.domain.Notes;
import com.task.financer.domain.User;
import com.task.financer.repos.NotesRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@Controller
public class NoteController {

    @Autowired
    private NotesRepo notesRepo;


    @GetMapping("/note")
    public String noteMain(Model model) {
        Iterable<Notes> notes = notesRepo.findAll();
        model.addAttribute("notes", notes);
        return "notes-user";
    }

    @PostMapping("/note/add")
    public String notePostAdd(
            @AuthenticationPrincipal User user,
            @RequestParam String title,
            @RequestParam String note, Map<String, Object> model
    ) {
        Notes notes = new Notes(title, note, user);
        notesRepo.save(notes);
        Iterable<Notes> notess = notesRepo.findAll();
        model.put("messages", notess);

        return "redirect:/note";
    }

    @GetMapping("/note/{id}/edit")
    public String noteEdit(@PathVariable(value = "id") long noteId, Model model) {
        Optional<Notes> note = notesRepo.findById(noteId);
        ArrayList<Notes> res = new ArrayList<>();
        note.ifPresent(res::add);
        model.addAttribute("notes", res);
        return "notesEdit";
    }

    @PostMapping("/note/{id}/edit")
    public String noteUpdate(@PathVariable(value = "id") long noteId,
                             @RequestParam String title,
                             @RequestParam String note, Model model) {
        Notes notes = notesRepo.findById(noteId).orElseThrow();
        notes.setNameNotes(title);
        notes.setTextNotes(note);
        notesRepo.save(notes);
        return "redirect:/note";
    }

    @GetMapping("/note/{id}")
    public String noteDetails(@PathVariable(value = "id") long noteId, Model model) {
        if (!notesRepo.existsById(noteId)) {
            return "redirect:/note";
        }
        Optional<Notes> note = notesRepo.findById(noteId);
        ArrayList<Notes> res = new ArrayList<>();
        note.ifPresent(res::add);
        model.addAttribute("notes", res);
        return "notes-details";
    }

    @GetMapping("/user-notes/{user}")
    public String userNotes(
            @AuthenticationPrincipal User currentUser,
            @PathVariable User user, Model model,
            @RequestParam(required = false) Notes notes
    ) {
        Set<Notes> note = user.getNotes();

        model.addAttribute("notes", notes);
        model.addAttribute("note", note);
        model.addAttribute("isCurrentUser", currentUser.equals(user));

        return "notes-details";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable(value = "id") long noteId, Model model) {
        Notes notes = notesRepo.findById(noteId).orElseThrow();
        notesRepo.delete(notes);
        return "redirect:/note";
    }
}


