package com.task.financer.controller;


import com.task.financer.domain.User;
import com.task.financer.repos.NotesRepo;
import com.task.financer.repos.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
/*@RequestMapping(path = "/") // This means URL's start with /demo (after Application path)*/
public class MainController {
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private NotesRepo notesRepo;

    @GetMapping("/")
    public String greeting(Map<String, Object> model) {
        return "greeting";
    }

    @GetMapping("/main")
    public String main(Map<String, Object> model) {
        model.put("some", "hello, azazka!");
        return "greeting";
    }


    @GetMapping(path = "/all")
    public @ResponseBody
    Iterable<User> getAllUsers() {
        // This returns a JSON or XML with the users
        return userRepo.findAll();
    }

}

