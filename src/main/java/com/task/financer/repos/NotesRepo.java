package com.task.financer.repos;

import com.task.financer.domain.Notes;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface NotesRepo extends CrudRepository<Notes, Long> {
    List<Notes> findById(String nameNotes);

    void deleteById(Long id);
}

