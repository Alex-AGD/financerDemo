<#macro notes>
    <div class="container mt-5">
        <h1>Add note</h1>
        <form action="/note/add" method="post">
            <div class="form-actions mb-3">
                <input type="text" name="title" placeholder="Введите имя заметки:" class="form-control">
            </div>

            <div class="form-actions mb-3">
                <textarea name="note" placeholder="Введите сообщени:" class="form-control"></textarea>
            </div>
            <input type="hidden" name="_csrf" value="${_csrf.token}"/>
            <button type="submit" class="btn btn-success">Добавить</button>
        </form>
    </div>

    <div class="container mt-5"></div>
</#macro>



