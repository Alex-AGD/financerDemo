<#import "parts/common.ftl" as c>
<#import "parts/login.ftl" as l>
<#import "parts/note-form.ftl" as n>

<@c.page>
    <table class="table table-bordered table-hover horizontal-align">

    <thead>
    <tr>
        <th style="width: 5%">ID</th>
        <th>Note Name</th>
        <th style="width: 60%">Text Message</th>
        <th style="width: 5%">Edit</th>
    </tr>
    </thead>

    <tbody>
    <div class="container mt-5 mb-5"></div>

    <h1>Редактор заметок</h1>
<form action="/note/${id}/edit" method="post">
    <#list notes as note>
    <tr>
        <td> ${note.id}</td>
        <td><input type="text" name="title" placeholder="${note.nameNotes}"></td>
        <td><input type="text" name="note" placeholder="${note.textNotes}"></td>
        <input type="hidden" name="_csrf" value="${_csrf.token}"/>
        <td><label><input type="submit" class="btn btn-success" value="Обновить"></label><br></td>
</form>
    </tbody>
</#list>
</@c.page>