<#import "parts/common.ftl" as c>
<#import "parts/login.ftl" as l>
<#import "parts/note-form.ftl" as n>

<@c.page>
    <table class="table table-bordered table-hover horizontal-align">
    <thead>
    <tr>
        <th style="width: 5%">ID</th>
        <th>Note Name</th>
        <th style="width: 60%">Message</th>
        <th style="width: 60%">Author</th>
        <th style="width: 5%">Edit</th>
        <th style="width: 5%">Delete</th>
    </tr>
    </thead>
    <tbody>

    <#list notes as note>
        <tr>
            <td> ${note.id}</td>
            <td> ${note.nameNotes} </td>
            <td>${note.textNotes}</td>
            <td>${note.authorName}</td>

            <td><a href="/note/${note.id}/edit"><i class="fa fa-pencil-square-o" style="font-size:20px"></i> Edit </a>
            </td>
            <td><a href="/delete/${note.id}"><i class="fa fa-trash" style="font-size:20px"></i> Delete </a></td>
        </tbody>
    <#else>
        <div class="alert  alert-primary" role="alert" style="width: 200px" >
            No message.
        </div>
    </#list>

</@c.page>
<@n.notes>

</@n.notes>